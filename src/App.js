import './App.css';
import Table from './Table';

function App() {
  return (
    <div className="App" style={{ marginLeft: "20%" }}>
      <br /><br />
      <Table />
    </div>
  );
}

export default App;
